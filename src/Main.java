import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);


        String[] stock = {"Mario Odyssey", "Super Smash Bros. Ultimate", "Luigi's Mansion 3", "Pokemon Sword", "Pokemon Shield"};
        HashMap<String, Integer> stockQuan = new HashMap<>();
        stockQuan.put(stock[0], 50);
        stockQuan.put(stock[1], 20);
        stockQuan.put(stock[2], 15);
        stockQuan.put(stock[3], 30);
        stockQuan.put(stock[4], 100);

        stockQuan.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left." );
        });

        ArrayList<String> topGames = new ArrayList<>();

        stockQuan.forEach((key,value) -> {
            if(value<=30){
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");

            }

        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);

    }
}